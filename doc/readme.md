
usage of onscreenapp
===

## dependence

- Onscreenapp depend on libhomescreen add agl-service-homescreen as below:
	- libhomescreen must have 'showWindow/hideWindow/replyShowWindow' event 
		and 'showWindow/hideWindow/replyShowWindow' interface.
	- agl-service-homescreen must have 'showWindow/hideWindow/replyShowWindow' verbs.

## sequence

show/hide onscreen sequence.
- ![showOnscreen.svg](parts/showOnscreen.svg)

## about sample 'onstestapp'
onstestapp is a sample to use onscreenapp.

### compile & install
- compile
	- when onscreenapp is compiled, this app's wgt file will exist at "onscreenapp/sample/package" which called onstestapp.wgt.

- install
		`afm-util install onstestapp.wgt;sync`

### onstestapp usage
- start onstestapp in launcher
- select onscreen type in groupbox
- press "Post" button

After clicked onscreen's button,you will see reply information at top area of onstestapp.
