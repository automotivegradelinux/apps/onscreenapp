import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import AGL.Demo.Controls 1.0

Item {
    id: onScreenVICS
    visible: true
    width: 1079
    height: 400
    scale: screenInfo.scale_factor()

    function qmlOnScreenParameter(message) {
        console.log(qsTr('OnScreenVICS:QML:System >>> qmlOnScreenMessage.'), message);
        var message_json = JSON.parse (message);
        vics_info.text = message_json.info;
    }

    Label {
        id: vics_info
        x: 40
        y: 72
        width: 1000
        height: 200
        color: "#eeeeec"
        text: "show vics inofo"
        font.pixelSize: 50
        textFormat: Text.AutoText
        font.wordSpacing: 0
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        id: button
        anchors.top: vics_info.bottom
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("OK")

        onClicked: {
            eventHandler.onScreenReply("OK");
        }
    }

}
