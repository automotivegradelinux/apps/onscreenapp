import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import AGL.Demo.Controls 1.0

Item {
    id: onScreenSystem
    visible: true
    width: 1079
    height: 400
    scale: screenInfo.scale_factor()

    function qmlOnScreenParameter(message) {
        console.log(qsTr('OnScreenSys:QML:System >>> qmlOnScreenMessage.'), message);
        var message_json = JSON.parse (message);
        var text = message_json.text
        label.text = text;
    }

    Image {
        id : background_image
        anchors.fill: parent
        anchors.topMargin: 0
        anchors.bottomMargin: 0
        source: "images/oval_1079x400.png"
    }

    Label {
        id: label
        x: 40
        y: 72
        width: 1000
        height: 100
        color: "#eeeeec"
        text: "system alert"
        font.pixelSize: 50
        textFormat: Text.AutoText
        font.wordSpacing: 0
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        id: button
//        x: 490
//        y: 250
        anchors.top: label.bottom
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("OK")

        onClicked: {
//            label.text = "system reject"
            eventHandler.onScreenReply("OK");
        }
    }

}
