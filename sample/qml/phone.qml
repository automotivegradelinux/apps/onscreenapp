import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import AGL.Demo.Controls 1.0

Item {
    id: onScreenPhone
    visible: true
    width: 1079
    height: 400
    scale: screenInfo.scale_factor()
    property string messageText: "Incoming Call"

    function qmlOnScreenParameter(message) {
        console.log(qsTr('OnScreenApp:QML:Phone >>> qmlOnScreenParameter.'), message);
        var message_json = JSON.parse (message);
        var text = message_json.status

        if(text === "incoming call")
        {
            messageText = "Incoming Call";
            answerButton.active = true;
            answerButton.checked = false;
        }
        else if(text === "call answered")
        {
            messageText = "Call Answered";
            answerButton.active = false;
            answerButton.checked = true;
        }
        else if(text === "call rejected")
        {
            messageText = "Call Rejected";
            answerButton.active = false;
            answerButton.checked = true;
        }
        else {
            messageText = text;
        }
    }

    Image {
        id : background_image
        anchors.fill: parent
        anchors.topMargin: 0
        anchors.bottomMargin: 0
        source: "images/heart_1079x400.png"
    }

   ToggleButton {
       id: answerButton
       x: 53
       y: 147
       width: 228
       height: 230
       onImage: 'images/disable.png'
       offImage: 'images/answer.png'
       property bool active: true

       onCheckedChanged: {
           if(!checked && !active) {
               checked = true;
           }
           if(active && checked)
           {
               messageText = "Call Answer"
               eventHandler.onScreenReply("call answer");
               active = false;
           }
       }
   }

   ImageButton {
       id: rejectButton
       x: 804
       y: 142
       width: 228
       height: 230
       offImage: 'images/reject.png'

       onClicked: {
           messageText = "Call Reject"
           eventHandler.onScreenReply("call reject");
       }
   }

    Label {
        x: 400
        y: 115
        width: 280
        height: 100
        color: "#000000"
        text: messageText
        textFormat: Text.AutoText
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 50
        font.weight: Font.ExtraBold
    }

    Label {
        x: 395
        y: 112
        width: 280
        height: 100
        color: "#6BFBFF"
        text: messageText
        textFormat: Text.AutoText
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 50
        font.weight: Font.DemiBold
    }

}
