import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import AGL.Demo.Controls 1.0

Item {
    id: onScreenMsg
    visible: true
    width: 1079
    height: 400
    scale: screenInfo.scale_factor()

    function qmlOnScreenParameter(message) {
        console.log(qsTr('OnScreenVICS:QML:System >>> qmlOnScreenMessage.'), message);
        var message_json = JSON.parse (message);
        data1.text = message_json.data1;
        data2.text = message_json.data2;
        data3.text = message_json.data3;
    }

    RowLayout {
        id: line1
        x: 40
        y: 72
        width: 1000
        height: 200
        spacing: 20
        Label {
            id: data1
            color: "#eeeeec"
            text: "show data1"
            font.pixelSize: 20
            textFormat: Text.AutoText
            font.wordSpacing: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: data2
            color: "#eeeeec"
            text: "show data2"
            font.pixelSize: 20
            textFormat: Text.AutoText
            font.wordSpacing: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: data3
            color: "#eeeeec"
            text: "show data3"
            font.pixelSize: 20
            textFormat: Text.AutoText
            font.wordSpacing: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

    }

    RowLayout {
        anchors.top: line1.bottom
        anchors.topMargin: 60
        anchors.horizontalCenter: parent.horizontalCenter
        Button {
            id: button1
            text: qsTr("Button1")
            onClicked: {
                eventHandler.onScreenReply("Button1");
            }
        }
        Button {
            id: button2
            text: qsTr("Button2")
            onClicked: {
                eventHandler.onScreenReply("Button2");
            }
        }
        Button {
            id: button3
            text: qsTr("Button3")
            onClicked: {
                eventHandler.onScreenReply("Button3");
            }
        }
    }

}
