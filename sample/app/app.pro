#
# Copyright (c) 2019 TOYOTA MOTOR CORPORATION
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TARGET = onstestapp
QT = quick quickcontrols2 qml

CONFIG += c++11 link_pkgconfig
PKGCONFIG += qlibwindowmanager qlibhomescreen
DESTDIR = $${OUT_PWD}/../package/root/bin

SOURCES = main.cpp \
    eventhandler.cpp

RESOURCES += \
    qml.qrc

HEADERS += \
    eventhandler.h

LIBS += -ljson-c

