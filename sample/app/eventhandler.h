/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include <QObject>
#include <string>
#include <QVariant>

#include <qlibhomescreen.h>
#include <qlibwindowmanager.h>
#include "hmi-debug.h"

#define ROLE_NAME "onstestapp"
#define APP_ID "onstestapp"

using namespace std;

class QQuickWindow;
class QQmlApplicationEngine;

class EventHandler : public QObject
{
    Q_OBJECT
public:
    explicit EventHandler(QObject *parent = 0);
    ~EventHandler();

    void init(int port, const char* token);
    void setQuickWindow(QQuickWindow *qw);
    static void* myThis;

    Q_INVOKABLE void showWindow(QString id, QString json);
    Q_INVOKABLE void hideWindow(QString id);

signals:
    void signalOnReplyShowWindow(QVariant val);

private:
    QLibHomeScreen *mp_hs;
    QLibWindowmanager* mp_wm;
    QQuickWindow *mp_qw;
};

#endif // EVENTHANDLER_H
