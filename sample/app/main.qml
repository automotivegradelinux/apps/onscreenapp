import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import AGL.Demo.Controls 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 1080
    height: 1487

    property string onsId: qsTr("onscreenapp")
    property string onsTitle: qsTr("One Button title")
    property string onsType: "critical"
    property string onsContents: qsTr("An operating system is a program that manages a computer’s hardware.")
    property string onsButton1: qsTr("Yes")
    property string onsButton2: qsTr("")
    property string onsButton3: qsTr("")
    property string postmsg: qsTr("")
    property string btndata: qsTr("")

    Label {
        id: title
        width: parent.width
        height: 40
        text: "OnScreen Test App"
        font.pixelSize: 40
        font.bold: true
        color: "white"
    }

    ColumnLayout {
        spacing: 20
        anchors.top: title.bottom
        anchors.topMargin: 40
        anchors.horizontalCenter: title.horizontalCenter

        // show received reply information area
        Flickable {
             id: flickable
             width: 800
             height: 320
             Layout.alignment: Qt.AlignCenter
             flickableDirection: Flickable.VerticalFlick
             boundsBehavior: Flickable.StopAtBounds

             TextArea.flickable: TextArea {
                 id: output
                 text: "show received reply information area\n...\n...\n...\n...\n"
                 font.pixelSize: 24
                 wrapMode: TextArea.Wrap
                 color: '#00ADDC'
             }

             ScrollBar.vertical: ScrollBar { }
         }

        // select onscreen type area
        GroupBox {
            label: Label {
                text: qsTr("Please select send OnScreen Message")
                font.pixelSize: 32
                font.bold: true
                color: "white"
            }

            width: 800
            height: 100
            Layout.alignment: Qt.AlignLeft

            RowLayout {
                anchors.fill: parent
                RadioButton {
                    text: qsTr("One Button")
                    font.pixelSize: 20
                    checked: true
                    onClicked: {
                        selectOneButton();
                    }
                }
                RadioButton {
                    text: qsTr("Two Buttons")
                    font.pixelSize: 20
                    onClicked: {
                        selectTwoButtons();
                    }
                }
                RadioButton {
                    text: qsTr("Three Buttons")
                    font.pixelSize: 20
                    onClicked: {
                        selectThreeButtons();
                    }
                }
                RadioButton {
                    text: qsTr("No Button")
                    font.pixelSize: 20
                    onClicked: {
                        selectNoButton();
                    }
                }
            }
        }

        // edit post message area
        GroupBox {
            label: Label {
                text: qsTr("Post Infomations")
                font.pixelSize: 32
                font.bold: true
                color: "white"
            }
            width: 800
            height: 400
            Layout.alignment: Qt.AlignLeft
            Layout.maximumWidth: 800
            Layout.maximumHeight: 400

            ColumnLayout {
                spacing:  20
                RowLayout {
                    spacing: 20
                    Label {
                        id: ons_title
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        text: qsTr("ons_title:")
                        font.pixelSize: 20
                        font.italic: true
                        color: '#00ADDC'
                    }

                    Label {
                        id: ons_title_data
                        text: onsTitle
                        font.pixelSize: 20
                        font.italic: true
                        anchors.left: ons_title.right
                        anchors.leftMargin: 20
                        color: '#00ADDC'
                        Layout.maximumWidth: 600
                        Layout.maximumHeight: 40
                        maximumLineCount: 1
                        wrapMode: Text.Wrap
                        elide: Text.ElideRight
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                    }
                }
                RowLayout {
                    spacing: 20
                    Label {
                        id: ons_type
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        text: qsTr("type:")
                        font.pixelSize: 20
                        font.italic: true
                        color: '#00ADDC'
                    }

                    Label {
                        id: ons_type_data
                        text: onsType
                        font.pixelSize: 20
                        font.italic: true
                        anchors.left: ons_type.right
                        anchors.leftMargin: 20
                        color: '#00ADDC'
                    }
                }
                RowLayout {
                    spacing: 20
                    Label {
                        id: ons_contents
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        text: qsTr("contents:")
                        font.pixelSize: 20
                        font.italic: true
                        color: '#00ADDC'
                    }
                    Label {
                        id: ons_contents_data
                        text: onsContents
                        font.pixelSize: 20
                        font.italic: true
                        anchors.left: ons_contents.right
                        anchors.leftMargin: 20
                        color: '#00ADDC'
                        Layout.maximumWidth: 600
                        Layout.maximumHeight: 200
                        maximumLineCount: 4
                        wrapMode: Text.Wrap
                        elide: Text.ElideRight
                        horizontalAlignment: Label.AlignLeft
                        verticalAlignment: Label.AlignVCenter
                    }
                }
                RowLayout {
                    spacing: 20
                    Label {
                        id: btn1
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        text: qsTr("Button1")
                        font.pixelSize: 20
                        font.italic: true
                        color: '#00ADDC'
                    }

                    Label {
                        id: btn1_data
                        text: onsButton1
                        font.pixelSize: 20
                        font.italic: true
                        anchors.left: btn1.right
                        anchors.leftMargin: 20
                        color: '#00ADDC'
                    }
                }
                RowLayout {
                    spacing: 20
                    Label {
                        id: btn2
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        text: qsTr("Button2")
                        font.pixelSize: 20
                        font.italic: true
                        color: '#00ADDC'
                    }
                    Label {
                        id: btn2_data
                        text: onsButton2
                        font.pixelSize: 20
                        font.italic: true
                        anchors.left: btn2.right
                        anchors.leftMargin: 20
                        color: '#00ADDC'
                    }
                }
                RowLayout {
                    spacing: 20
                    Label {
                        id: btn3
                        anchors.left: parent.left
                        anchors.leftMargin: 30
                        text: qsTr("Button3")
                        font.pixelSize: 20
                        font.italic: true
                        color: '#00ADDC'
                    }
                    Label {
                        id: btn3_data
                        text: onsButton3
                        font.pixelSize: 20
                        font.italic: true
                        anchors.left: btn3.right
                        anchors.leftMargin: 20
                        color: '#00ADDC'
                    }
                }
            }
        }

        // post button
        Button {
            id: poster
            text: "Post"
            Layout.alignment: Qt.AlignCenter
            onClicked: {
                postMessage();
            }
        }
    }

    function selectOneButton() {
        console.log("select one button!")
        onsTitle = qsTr("One Button title")
        onsType = qsTr("critical")
        onsContents = qsTr("An operating system is a program that manages a computer’s hardware.")
        onsButton1 = qsTr("Yes")
        onsButton2 = qsTr("")
        onsButton3 = qsTr("")
    }

    function selectTwoButtons() {
        console.log("select two buttons!")
        onsTitle = qsTr("Two Buttons title")
        onsType = qsTr("exclamation")
        onsContents = qsTr("Beforewe can explore the details of computer system operation, we need to know something about system structure. We thus discuss the basic functions of system startup, I/O, and storage early in this chapter. We also describe the basic computer architecture that makes it possible to write a functional operating system.")
        onsButton1 = qsTr("Yes")
        onsButton2 = qsTr("No")
        onsButton3 = qsTr("")
    }

    function selectThreeButtons() {
        console.log("select three buttons!")
        onsTitle = qsTr("Three Buttons title")
        onsType = qsTr("information")
        onsContents = qsTr("We can also view a computer system as consisting of hardware, software,and data. The operating system provides the means for proper use of these resources in the operation of the computer system.")
        onsButton1 = qsTr("Yes")
        onsButton2 = qsTr("Abort")
        onsButton3 = qsTr("No")
    }

    function selectNoButton() {
        console.log("select no button!")
        onsTitle = qsTr("No Button title,very long title beyond screen wide which will show ellipsis at the end")
        onsType = qsTr("question")
        onsContents = qsTr("Recently, many varieties of mobile computers, such as smartphones and tablets, have come into fashion. Most mobile computers are standalone units for individual users. Quite often, they are connected to networks through cellular or other wireless technologies. Increasingly, these mobile devices are replacing desktop and laptop computers for people who are primarily interested in using computers for e-mail and web browsing. The user interface for mobile computers generally features a touch screen, where the user interacts with the system by pressing and swiping fingers across the screen rather than using a physical keyboard and mouse.")
        onsButton1 = qsTr("")
        onsButton2 = qsTr("")
        onsButton3 = qsTr("")
    }

    function postMessage() {
        console.log("poster pressed")
        btndata = ""
        postmsg = "{\"title\": \"" + onsTitle + "\"," + "\"type\": \"" + onsType + "\"," + "\"contents\": \"" + onsContents + "\"";
        if (onsButton1 != "") {
            btndata = "\"" + onsButton1 + "\"";
        }
        if (onsButton2 != "") {
            if (btndata != "")
                btndata += ",";
            btndata += "\"" + onsButton2 + "\"";
        }
        if (onsButton3 != "") {
            if (btndata != "")
                btndata += ",";
            btndata += "\"" + onsButton3 + "\"";
        }

        if(btndata != "")
            postmsg += ",\"buttons\":[" + btndata + "]}"
        else
            postmsg += "}"

        eventHandler.showWindow(onsId, postmsg);
    }

    function qmlOnReplyShowWindow(text) {
        console.log("onstestapp received:",text);
        output.text = text;
    }
}
