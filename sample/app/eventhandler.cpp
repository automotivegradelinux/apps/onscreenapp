/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <functional>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQuickWindow>
//#include <QtQml/QQmlContext>
#include <QQmlContext>
#include <QtQml/QQmlApplicationEngine>
#include "eventhandler.h"

void* EventHandler::myThis = 0;

const char _drawing_name[] = "drawing_name";

EventHandler::EventHandler(QObject *parent) :
    QObject(parent),
    mp_hs(NULL),
    mp_wm(NULL),
    mp_qw(NULL)
{

}

EventHandler::~EventHandler()
{
    if (mp_hs != NULL) {
        delete mp_hs;
    }
    if (mp_wm != NULL) {
        delete mp_wm;
    }
}

void EventHandler::init(int port, const char *token)
{
    myThis = this;
    mp_wm = new QLibWindowmanager();
    mp_wm->init(port, token);

    mp_hs = new QLibHomeScreen();
    mp_hs->init(port, token);

    mp_hs->set_event_handler(QLibHomeScreen::Event_ShowWindow, [this](json_object *object){
        this->mp_wm->activateWindow(ROLE_NAME, "normal");
        HMI_DEBUG(APP_ID, "received showWindow event, end!, line=%d", __LINE__);
    });

    mp_hs->set_event_handler(QLibHomeScreen::Event_ReplyShowWindow, [this](json_object *object){
        HMI_DEBUG(APP_ID, "got Event_ReplyShowWindow!\n");
        const char* msg = json_object_to_json_string(object);
        emit this->signalOnReplyShowWindow(msg);
    });

    if (mp_wm->requestSurface(ROLE_NAME) != 0) {
        HMI_DEBUG(APP_ID, "!!!!LayoutHandler requestSurface Failed!!!!!");
        exit(EXIT_FAILURE);
    }

    // Create an event callback against an event type. Here a lambda is called when SyncDraw event occurs
    mp_wm->set_event_handler(QLibWindowmanager::Event_SyncDraw, [this](json_object *object) {
        HMI_DEBUG(APP_ID, "Surface got syncDraw!");
        this->mp_wm->endDraw(ROLE_NAME);
    });

    mp_wm->set_event_handler(QLibWindowmanager::Event_Visible, [this](json_object *object) {
        struct json_object *value;
        json_object_object_get_ex(object, _drawing_name, &value);
        const char *name = json_object_get_string(value);

        HMI_DEBUG(APP_ID, "Event_Active kKeyDrawingName = %s", name);
    });

    mp_wm->set_event_handler(QLibWindowmanager::Event_Invisible, [this](json_object *object) {
        struct json_object *value;
        json_object_object_get_ex(object, _drawing_name, &value);
        const char *name = json_object_get_string(value);

        HMI_DEBUG(APP_ID, "Event_Inactive kKeyDrawingName = %s", name);
    });

    HMI_DEBUG(APP_ID, "LayoutHander::init() finished.");
}

void EventHandler::setQuickWindow(QQuickWindow *qw)
{
    mp_qw = qw;
    QObject::connect(mp_qw, SIGNAL(frameSwapped()), mp_wm, SLOT(slotActivateSurface()));
}

void EventHandler::showWindow(QString id, QString json)
{
    if(json.isNull())
        mp_hs->tapShortcut(id);
    else
        mp_hs->showWindow(id.toStdString().c_str(), json_tokener_parse(json.toStdString().c_str()));
}

void EventHandler::hideWindow(QString id)
{
    mp_hs->hideWindow(id.toStdString().c_str());
}
