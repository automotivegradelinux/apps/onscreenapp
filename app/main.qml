/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import AGL.Demo.Controls 1.0
import OnScreenModel 1.0

Window {
    id: root
    flags: Qt.FramelessWindowHint
    visible: true
    x: 0
    y: 	218
    width: 1080
    height: 1488
    color: '#00000000'

    Onscreen {
        id: ons
        anchors.centerIn: parent
        visible: true
        scale: 0

        property string dsp_sts: "hide"
        property string dsp_title: ""
        property string dsp_icon: ""
        property string dsp_contents: ""
        property int btnNum: 0
        property string btn1Name: ""
        property string btn2Name: ""
        property string btn3Name: ""

        states: [
            State {
                name: 'active'
                when: ons.dsp_sts == "show"
                PropertyChanges {
                    target: ons
                    scale: 1
                    z: 1
                }
            },
            State {
                name: 'inactive'
                when: ons.dsp_sts == "hide"
                PropertyChanges {
                    target: ons
                    scale: 0
                    z: -1
                }
            }
        ]
        transitions: Transition {
            NumberAnimation {
                properties: 'scale';
                duration: 300;
                easing.type: Easing.Linear
                alwaysRunToEnd: true
            }
        }
    }

    OnScreenModel {
        id: onscreenModel
    }

    Timer {
        id: ons_timer
        interval: 3000
        onTriggered: {
            hideOnScreen();
            clearOnScreenModel();
        }
    }

    Connections {
        target: ons
        onScaleChanged : {
            if(ons.scale == 0) {
                console.log(qsTr('hide animation finished'));
                eventHandler.deactivateWindow();
            }
        }
    }

    function showOnScreen() {
        console.log(qsTr('show onscreenapp'));
        ons.dsp_title = onscreenModel.getTitle()
        ons.dsp_icon = "../images/" + onscreenModel.getType() + ".svg"
        ons.dsp_contents = onscreenModel.getContents()
        ons.btnNum = onscreenModel.buttonNum()
        ons.btn1Name = onscreenModel.buttonName(0)
        ons.btn2Name = onscreenModel.buttonName(1)
        ons.btn3Name = onscreenModel.buttonName(2)
        ons_timer.running =  ons.btnNum > 0 ? false : true
        ons.dsp_sts = "show"
    }

    function hideOnScreen() {
        console.log(qsTr('hide onscreenapp'));
        ons.dsp_sts = "hide"
        ons_timer.running = false
    }

    function setOnScreenModel(data) {
        console.log(qsTr('onscreenapp >>> setModel status: ' + data));
        onscreenModel.setModel(data)
    }
}
