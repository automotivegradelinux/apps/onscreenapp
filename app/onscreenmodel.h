/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ONSCREENMODEL_H
#define ONSCREENMODEL_H

#include <QObject>
#include <QVariant>
#include <QStringList>


class OnScreenModel : public QObject
{
    Q_OBJECT
public:
    explicit OnScreenModel(QObject *parent = nullptr){}
    ~OnScreenModel() = default;

    Q_INVOKABLE QString getTitle(void) const {return m_title;}
    Q_INVOKABLE QString getType(void) const {return m_type;}
    Q_INVOKABLE QString getContents(void) const {return m_contents;}
    Q_INVOKABLE int buttonNum(void) const {return m_buttons.size();}
    Q_INVOKABLE QString buttonName(int index) const;
    Q_INVOKABLE void setModel(QVariant data);

private:
    void clearModel(void);

    QString m_title;
    QString m_type;
    QString m_contents;
    QStringList m_buttons;
};

#endif // ONSCREENMODEL_H
