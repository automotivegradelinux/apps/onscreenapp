#
# Copyright (c) 2019 TOYOTA MOTOR CORPORATION
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TEMPLATE = app
TARGET = onscreenapp
QT += quickcontrols2 quick qml
CONFIG += c++11 link_pkgconfig
DESTDIR = $${OUT_PWD}/../package/root/bin
PKGCONFIG += qlibwindowmanager libhomescreen

CONFIG(release, debug|release) {
    QMAKE_POST_LINK = $(STRIP) --strip-unneeded $(TARGET)
}

HEADERS += \
    eventhandler.h \
    onscreenmodel.h

SOURCES += \
    eventhandler.cpp \
    main.cpp \
    onscreenmodel.cpp

RESOURCES += \
	qml.qrc \
    images/images.qrc

LIBS += -ljson-c
