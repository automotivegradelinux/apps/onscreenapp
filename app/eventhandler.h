/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HOMESCREENHANDLER_H
#define HOMESCREENHANDLER_H

#include <QObject>
#include <string>
#include <QVariant>
#include <QPair>
#include <libhomescreen.hpp>
#include <qlibwindowmanager.h>
#include "hmi-debug.h"

#define APP_ID "onscreenapp"

class QQmlApplicationEngine;

class EventHandler : public QObject
{
    Q_OBJECT
public:
    explicit EventHandler(QObject *parent = 0);
    ~EventHandler();
    EventHandler(const EventHandler&) = delete;
    EventHandler& operator=(const EventHandler&) = delete;

    void init(int port, const char* token);
    void onRep(struct json_object* reply_contents);

    static void* myThis;
    static void onRep_static(struct json_object* reply_contents);

    Q_INVOKABLE void deactivateWindow();
    Q_INVOKABLE void onScreenReply(const QString &ons_title, const QString &btn_name);

signals:
    void updateModel(QVariant data);
    void showOnScreen();
    void hideOnScreen();

private:
    enum {
        HIDING = 0,
        SHOWING,
        SWAPPING
    };

    int getDisplayStatus() {return m_dsp_sts;}
    void setDisplayStatus(int sts) {m_dsp_sts = sts;}
    void activateWindow(const char *role, const char *area = "normal.full");

    LibHomeScreen *mp_hs;
    QLibWindowmanager* mp_wm;
    QPair<QString, QString> m_req, m_dsp;
    int m_dsp_sts = HIDING;
};
#endif // HOMESCREENHANDLER_H
