/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <functional>
#include <QUrl>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQmlContext>
#include <QtQml/QQmlApplicationEngine>
#include <cstring>
#include <QFileInfo>

#include "eventhandler.h"

const char _myrole[] = "on_screen";
const char _parameter[] = "parameter";
const char _replyto[] = "replyto";
const char _onscreen_title[] = "onscreenTitle";
const char _button_name[] = "buttonName";
const char _drawing_name[] = "drawing_name";
const char _application_id[] = "application_id";


void* EventHandler::myThis = 0;

EventHandler::EventHandler(QObject *parent) :
    QObject(parent),
    mp_hs(nullptr),
    mp_wm(nullptr),
    m_dsp_sts(false)
{
}

EventHandler::~EventHandler()
{
    if (mp_hs != nullptr) {
        delete mp_hs;
    }
    if (mp_wm != nullptr) {
        delete mp_wm;
    }
}

void EventHandler::init(int port, const char *token)
{
    myThis = this;
    mp_wm = new QLibWindowmanager();
    mp_wm->init(port, token);

    mp_hs = new LibHomeScreen();
    mp_hs->init(port, token);

    mp_hs->registerCallback(nullptr, EventHandler::onRep_static);
    mp_hs->set_event_handler(LibHomeScreen::Event_ShowWindow, [this](json_object *object){
        /*
        {
            "application_id": "onscreenapp",
            "parameter": {
                "title": "onscreen title",
                "type": "critical,exclamation,question,information",
                "contents": "message contents",
                "buttons": ["button_name1", "button_name2", "button_name3"],
                "replyto":"caller application id"
            }
        } */
        HMI_DEBUG(APP_ID, "recived json message is[%s]", json_object_get_string(object));

        struct json_object *param;
        if(!json_object_object_get_ex(object, _parameter, &param)
        || json_object_get_type(param) != json_type_object) {
            HMI_DEBUG(APP_ID, "parameter error!");
            return;
        }

        struct json_object *replyid;
        const char *replyto = nullptr;
        if(json_object_object_get_ex(param, _replyto, &replyid))
            replyto = json_object_get_string(replyid);
        if(replyto == nullptr) {
            HMI_DEBUG(APP_ID, "received replyto is null!");
            return;
        }
        m_req = qMakePair(QString(replyto), QString(json_object_to_json_string(param)));

        if (this->getDisplayStatus() == HIDING) {
            this->activateWindow(_myrole, "on_screen");
        }
        else if(this->getDisplayStatus() == SHOWING) {
            this->setDisplayStatus(SWAPPING);
            emit this->hideOnScreen();
        }
        else {
            HMI_DEBUG(APP_ID, "onscreen swapping!");
        }
        HMI_DEBUG(APP_ID, "received showWindow event, end!, line=%d", __LINE__);
    });

    mp_hs->set_event_handler(LibHomeScreen::Event_HideWindow, [this](json_object *object){
        emit this->hideOnScreen();
        HMI_DEBUG(APP_ID, "hideWindow json_object=%s", json_object_get_string(object));
    });

    if (mp_wm->requestSurface(_myrole) != 0) {
        HMI_DEBUG(APP_ID, "!!!!LayoutHandler requestSurface Failed!!!!!");
        exit(EXIT_FAILURE);
    }

    mp_wm->set_event_handler(QLibWindowmanager::Event_SyncDraw, [this](json_object *object) {
        HMI_DEBUG(APP_ID, "Surface %s got syncDraw!", _myrole);
        this->mp_wm->endDraw(QString(_myrole));
    });

    mp_wm->set_event_handler(QLibWindowmanager::Event_Visible, [this](json_object *object) {
        struct json_object *value;
        json_object_object_get_ex(object, _drawing_name, &value);
        const char *name = json_object_get_string(value);
        if(!strcasecmp(_myrole, name)){
            this->setDisplayStatus(SHOWING);
            this->m_dsp = this->m_req;
            this->updateModel(QVariant(this->m_dsp.second));
            emit this->showOnScreen();
        }

        HMI_DEBUG(APP_ID, "Event_Visible kKeyDrawingName = %s", name);
    });

    mp_wm->set_event_handler(QLibWindowmanager::Event_Invisible, [this](json_object *object) {
        struct json_object *value;
        json_object_object_get_ex(object, _drawing_name, &value);
        const char *name = json_object_get_string(value);

        HMI_DEBUG(APP_ID, "Event_Invisible kKeyDrawingName = %s", name);
    });

    HMI_DEBUG(APP_ID, "LayoutHander::init() finished.");
}

void EventHandler::onRep_static(struct json_object* reply_contents)
{
    static_cast<EventHandler*>(EventHandler::myThis)->onRep(reply_contents);
}

void EventHandler::onRep(struct json_object* reply_contents)
{
    const char* str = json_object_to_json_string(reply_contents);
    HMI_DEBUG(APP_ID, "EventHandler::onReply %s", str);
}

void EventHandler::activateWindow(const char *role, const char *area)
{
    HMI_DEBUG(APP_ID, "EventHandler::activateWindow()");
    mp_wm->activateWindow(role, area);
}

void EventHandler::deactivateWindow()
{
    HMI_DEBUG(APP_ID, "EventHandler::deactivateWindow()");
    if(getDisplayStatus() == SWAPPING) {
        setDisplayStatus(SHOWING);
        m_dsp = m_req;
        updateModel(QVariant(this->m_dsp.second));
        emit showOnScreen();
    }
    else {
        this->setDisplayStatus(HIDING);
        mp_wm->deactivateWindow(_myrole);
    }
}

void EventHandler::onScreenReply(const QString &ons_title, const QString &btn_name)
{
    HMI_DEBUG(APP_ID, "ons_title=%s btn_name=%s", ons_title.toStdString().c_str(), btn_name.toStdString().c_str());
    emit this->hideOnScreen();

    struct json_object* j_param = json_object_new_object();
    json_object_object_add(j_param, _onscreen_title, json_object_new_string(ons_title.toStdString().c_str()));
    json_object_object_add(j_param, _button_name, json_object_new_string(btn_name.toStdString().c_str()));
    mp_hs->replyShowWindow(m_dsp.first.toStdString().c_str(), j_param);
}
