/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "onscreenmodel.h"
#include "hmi-debug.h"
#include <json-c/json.h>

const char _modelName[] = "OnScreenModel";
const char _title[] = "title";
const char _type[] = "type";
const char _contents[] = "contents";
const char _buttons[] = "buttons";


void OnScreenModel::setModel(QVariant data)
{
    HMI_DEBUG(_modelName, "setModel start!");
    clearModel();
    struct json_object *j_title = nullptr, *j_type = nullptr, *j_contents = nullptr, *j_buttons = nullptr;
    struct json_object *j_param = json_tokener_parse(data.toString().toStdString().c_str());
    if(json_object_object_get_ex(j_param, _title, &j_title)) {
        m_title = json_object_get_string(j_title);
    }
    else {
        HMI_DEBUG(_modelName, "title input is null");
    }

    if(json_object_object_get_ex(j_param, _type, &j_type)) {
        m_type = json_object_get_string(j_type);
    }
    else {
        HMI_DEBUG(_modelName, "type input is null");
    }

    if(json_object_object_get_ex(j_param, _contents, &j_contents)) {
        m_contents = json_object_get_string(j_contents);
    }
    else {
        HMI_DEBUG(_modelName, "contents input is null");
    }

    if(json_object_object_get_ex(j_param, _buttons, &j_buttons)) {
        if(json_object_get_type(j_buttons) != json_type_array) {
            HMI_DEBUG(_modelName, "buttons josn type isn't array!");
        }
        else {
            m_buttons.clear();
            int len = json_object_array_length(j_buttons);
            struct json_object *json_tmp = nullptr;
            for (int i = 0; i < len; i++) {
                json_tmp = json_object_array_get_idx(j_buttons, i);
                m_buttons << QString(json_object_get_string(json_tmp));
            }
        }
    }
    else {
        HMI_DEBUG("OnScreenModel", "buttons input is null");
    }
    HMI_DEBUG(_modelName, "setModel end!titile=%s,type=%s,contents=%s,btnNum=%d",
              m_title.toStdString().c_str(), m_type.toStdString().c_str(), m_contents.toStdString().c_str(), m_buttons.size());
}

QString OnScreenModel::buttonName(int index) const
{
    if(index >= m_buttons.size())
        return QString("");
    else
        return m_buttons[index];
}

void OnScreenModel::clearModel(void)
{
    m_title = "";
    m_type = "";
    m_contents = "";
    m_buttons.clear();
}
