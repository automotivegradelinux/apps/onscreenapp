/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QGuiApplication>
#include <QCommandLineParser>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQml/qqml.h>
#include <QQuickWindow>
#include <QtQuickControls2/QQuickStyle>

#include "eventhandler.h"
#include "onscreenmodel.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QCoreApplication::setOrganizationDomain("LinuxFoundation");
    QCoreApplication::setOrganizationName("AutomotiveGradeLinux");
    QCoreApplication::setApplicationName("Onscreenapp");
    QCoreApplication::setApplicationVersion("0.1.0");

    QQuickStyle::setStyle("AGL");

    QCommandLineParser parser;
    parser.addPositionalArgument("port", app.translate("main", "port for binding"));
    parser.addPositionalArgument("secret", app.translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);
    QStringList positionalArguments = parser.positionalArguments();

    int port = 1700;
    QString token = "wm";

    if (positionalArguments.length() == 2) {
        port = positionalArguments.takeFirst().toInt();
        token = positionalArguments.takeFirst();
    }

    HMI_DEBUG(APP_ID, "port = %d, token = %s", port, token.toStdString().c_str());

    QQmlApplicationEngine engine;
    EventHandler *eventHandler = new EventHandler();
    eventHandler->init(port, token.toStdString().c_str());
    engine.rootContext()->setContextProperty("eventHandler", eventHandler);
    qmlRegisterType<OnScreenModel>("OnScreenModel", 1, 0, "OnScreenModel");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        HMI_DEBUG(APP_ID, "Fatal Error, rootObject is empty!");
        return -1;
    }

    QObject *root = engine.rootObjects().first();
    QQuickWindow *window = qobject_cast<QQuickWindow *>(root);
    QObject::connect(eventHandler, SIGNAL(updateModel(QVariant)), window, SLOT(setOnScreenModel(QVariant)));
    QObject::connect(eventHandler, SIGNAL(showOnScreen()), window, SLOT(showOnScreen()));
    QObject::connect(eventHandler, SIGNAL(hideOnScreen()), window, SLOT(hideOnScreen()));

    HMI_DEBUG(APP_ID, "onscreenapp started!");
    return app.exec();
}

